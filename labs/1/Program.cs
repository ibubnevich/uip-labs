﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            double y, x, result;
            Console.WriteLine("Input x");
            x = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Input y");
            y = Convert.ToDouble(Console.ReadLine());

            result = calcFunc(x, y);

            Console.WriteLine("Result:" + result);

            Console.ReadLine();
        }

        static double calcFunc(double x, double y)
        {
            double numerator =(3 + Math.Pow(Math.E, y - 1)) / (1); ;
            double denominator = 1 + Math.Pow(x, 2) * Math.Abs(y - Math.Tan(x));
            double result = numerator / denominator;
            return result;
        }
    }
}
