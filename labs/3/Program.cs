﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    /*
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input count of elements");
            int countOfElements = Convert.ToInt32(Console.ReadLine());
            int[] initialArray = new int[countOfElements];
            Console.WriteLine("Input elements one by one");
            for (int i = 0; i < countOfElements; i++)
            {
                Console.Write("a[" + i + "]=");
                initialArray[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Initial array");
            outputArray(initialArray);
            int[] resultArray = getArrayWithEvenElements(initialArray);

            if (resultArray.Length == 0)
            {
                Console.WriteLine("Array doesn't contain even elements");
            }
            else
            {
                Console.WriteLine("Result array:");
                outputArray(resultArray);
            }

            Console.ReadLine();
        }

        static int[] getArrayWithEvenElements(int[] array)
        {

            List<int> list = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 2 == 0)
                {
                    list.Add(array[i]);
                }

            }
            int[] result = list.ToArray();
            return result;

        }


        static void outputArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + "  ");
            }
            Console.WriteLine();
        }
    }
}
