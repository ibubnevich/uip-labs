﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _8
{
    public partial class Form2 : Form
    {
        lab8Entities entities;

        bool isEdit = false;

        int id; 
        public Form2()
        {
            InitializeComponent();
            textBox1.ReadOnly = false;
        }


        public Form2(int id, double x, double y, double z)
        {
            InitializeComponent();
            textBox1.ReadOnly = true;
            isEdit = true;
            textBox1.Text = id.ToString();
            textBox2.Text = x.ToString();
            textBox3.Text = y.ToString();
            textBox4.Text = z.ToString();
            this.id = id;

        }


        private void button1_Click(object sender, EventArgs e)
        {
            entities = new lab8Entities();

            if (isEdit)
            {
                Vectors vector = entities.VectorsES.Find(id);
                
                vector.x_coordinate = Convert.ToDouble(textBox2.Text);
                vector.y_coordinate = Convert.ToDouble(textBox3.Text);
                vector.z_coordinate = Convert.ToDouble(textBox4.Text);
            }
            else
            {
                Vectors vector = new Vectors();
                vector.ID = Convert.ToInt32(textBox1.Text);
                vector.x_coordinate = Convert.ToDouble(textBox2.Text);
                vector.y_coordinate = Convert.ToDouble(textBox3.Text);
                vector.z_coordinate = Convert.ToDouble(textBox4.Text);
                entities.VectorsES.Add(vector);

            }

            entities.SaveChanges();
            entities.Dispose();
            this.Close();


        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
