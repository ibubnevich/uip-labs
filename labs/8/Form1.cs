﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _8
{
    public partial class Form1 : Form
    {
        lab8Entities entities;
        public Form1()
        {
           
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'lab8DataSet.Vectors' table. You can move, or remove it, as needed.
            this.vectorsTableAdapter.Fill(this.lab8DataSet.Vectors);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            updateTable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();

            updateTable();

        }


         private void updateTable()
        {
            entities = new lab8Entities();
            dataGridView1.DataSource = entities.VectorsES.ToList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dataGridView1.SelectedCells[0].Value);
            int x = Convert.ToInt32(dataGridView1.SelectedCells[1].Value);
            int y = Convert.ToInt32(dataGridView1.SelectedCells[2].Value);
            int z = Convert.ToInt32(dataGridView1.SelectedCells[3].Value);

            Form2 form2 = new Form2(id,x,y,z);
            form2.ShowDialog();
            updateTable();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            entities = new lab8Entities();

            Vectors vector = entities.VectorsES.Find(Convert.ToInt32(dataGridView1.SelectedCells[0].Value));
            entities.VectorsES.Remove(vector);
            entities.SaveChanges();
            entities.Dispose();
            updateTable();
        }
    }
}
