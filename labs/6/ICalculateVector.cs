using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6
{
    public interface ICalculateVector
    {
        double Length();
        double VectorsCos(Vector v1, Vector v2);
    }
}
