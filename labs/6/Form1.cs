using System;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

namespace _6
{
    public partial class Form1 : Form
    {

        private List<Vector> vectorLst;

        public Form1()
        {
            InitializeComponent();

            vectorLst = new List<Vector>();
        }

        private bool CanAdd()
        {
            return textBoxX.Text.Length > 0 && textBoxY.Text.Length > 0 && textBoxZ.Text.Length > 0;
        }

        private void textBoxX_TextChanged(object sender, EventArgs e)
        {
            buttonAddVector.Enabled = CanAdd();
        }

        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            buttonAddVector.Enabled = CanAdd();
        }

        private void textBoxZ_TextChanged(object sender, EventArgs e)
        {
            buttonAddVector.Enabled = CanAdd();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (FileStream fs = new FileStream("vectors.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, vectorLst);

                textBoxVectors.AppendText("Objects serialized" + Environment.NewLine);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (FileStream fs = new FileStream("vectors.dat", FileMode.OpenOrCreate))
            {
                vectorLst = (List<Vector>)formatter.Deserialize(fs);

                foreach (Vector item in vectorLst)
                {
                    textBoxVectors.AppendText("Vector load! x: " + item.X + " y: " + item.Y + " z: " + item.Z + Environment.NewLine);
                }
            }

            loadToolStripMenuItem.Enabled = false;
        }

        private void buttonAddVector_Click(object sender, EventArgs e)
        {
            Vector vector = new Vector(Convert.ToDouble(textBoxX.Text), Convert.ToDouble(textBoxY.Text), Convert.ToDouble(textBoxZ.Text));

            vectorLst.Add(vector);

            textBoxX.ResetText();
            textBoxY.ResetText();
            textBoxZ.ResetText();

            textBoxVectors.AppendText("Vector add! x: " + vector.X + " y: " + vector.Y + " z: " + vector.Z + Environment.NewLine);
        }
    }
}
