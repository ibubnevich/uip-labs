﻿using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace _8hibernate
{
    class NHibernateSession
    {
        public static object HttpContext { get; private set; }

        public static NHibernate.ISession OpenSession()
        {
            var configuration = new Configuration();
            var configurationPath = @"D:\BSUIR\3 курс\уип\uip-labs\labs\8hibernate\hibernate.cfg.xml";
            configuration.Configure(configurationPath);

            configuration = Fluently
        .Configure(configuration)
        .Mappings(cfg =>
        {
            cfg.FluentMappings.AddFromAssemblyOf<Vector>();
        })
        .BuildConfiguration();


            //configuration.AddAssembly(typeof(Vector).Assembly);

            ISessionFactory sessionFactory = configuration.BuildSessionFactory();
            return sessionFactory.OpenSession();
        }


        /*
        public static NHibernate.ISession OpenSession()
        {
            var configuration = new Configuration();
            var configurationPath = @"D:\BSUIR\3 курс\уип\uip-labs\labs\8hibernate\hibernate.cfg.xml";
            configuration.Configure(configurationPath);
            var bookConfigurationFile = @"D:\BSUIR\3 курс\уип\uip-labs\labs\8hibernate\Models\Vector.hbm.xml";
            configuration.AddFile(bookConfigurationFile);
            ISessionFactory sessionFactory = configuration.BuildSessionFactory();
            return sessionFactory.OpenSession();
        }*/
    }
}
