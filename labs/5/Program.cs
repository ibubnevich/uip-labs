using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector vector = new Vector(1, 2, 3);

            vector.OnDataChanged += MessageNotifier.MessageDataChanged;

            vector.X = 10;
            vector.Y = 20;
            vector.Z = 50;

            Console.ReadLine();

        }
    }
}
