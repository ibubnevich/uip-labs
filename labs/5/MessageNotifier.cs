using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5
{
    class MessageNotifier
    {
        public static void MessageDataChanged(string sName, double from, double to)
        {
            Console.WriteLine("Data changed on property: " + sName + " from: " + from + " to: " + to);
        }

    }
}
