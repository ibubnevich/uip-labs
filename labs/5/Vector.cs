using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5
{
    public class Vector : ICalculateVector
    {
        double x, y, z;
        public double X { set { OnDataChanged("x", x, value); x = value; } get { return x; } }
        public double Y { set { OnDataChanged("y", y, value); y = value; } get { return y; } }
        public double Z { set { OnDataChanged("z", z, value); z = value; } get { return z; } }


        public delegate void DataChanged(string sName, double from, double to);

        public event DataChanged OnDataChanged;

        public Vector(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double Length()
        {
            return Math.Sqrt((Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2)));
        }

        public double VectorsCos(Vector v1, Vector v2)
        {

            return (v1.x * v2.x + v1.y * v2.y + v1.z * v1.z) / (v1.Length() * v2.Length());

        }

        public void Output()
        {
            Console.WriteLine("Координата х: " + this.x);
            Console.WriteLine("Координата y: " + this.y);
            Console.WriteLine("Координата z: " + this.z);
        }


        public static Vector operator +(Vector v1, Vector v2)
        {
            return new Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }

        public static Vector operator -(Vector v1, Vector v2)
        {
            return new Vector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }

        public static double operator *(Vector v1, Vector v2)
        {
            return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
        }
    }
}
